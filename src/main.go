package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/yanzay/tbot"
)

// Advice struct for the API response
type Advice struct {
	Slip struct {
		Advice string `json:"advice"`
		SlipID string `json:"slip_id"`
	} `json:"slip"`
}

// Insult struct for the API response
type Insult struct {
	Number    string `json:"number"`
	Language  string `json:"language"`
	Insult    string `json:"insult"`
	Created   string `json:"created"`
	Shown     string `json:"shown"`
	Createdby string `json:"createdby"`
	Active    string `json:"active"`
	Comment   string `json:"comment"`
}

// Shibe struct for the shibe.online
type Shibe []string

// Token struct for the Token
type Token struct {
	Links struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
	} `json:"_links"`
	LastUpdatedTimestamp int64 `json:"last_updated_timestamp"`
	Price                int   `json:"price"`
}

// Function to insert a dot every X chars
func insertNth(s string, n int) string {
	var buffer bytes.Buffer
	var n1 = n - 1
	var l1 = len(s) - 1
	for i, rune := range s {
		buffer.WriteRune(rune)
		if i%n == n1 && i != l1 {
			buffer.WriteRune('.')
		}
	}
	return buffer.String()
}

func main() {
	token := "APITOKEN"
	// Create new telegram bot server using token
	bot, err := tbot.NewServer(token)
	if err != nil {
		log.Fatal(err)
	}

	// Use whitelist for Auth middleware, allow to interact only with user1 and user2
	// whitelist := []string{"yanzay", "user2"}
	// bot.AddMiddleware(tbot.NewAuth(whitelist))

	// Handle with BiftHandler function
	bot.HandleFunc("/biftdus", BiftHandler)

	// Handle with TokenHandler function
	bot.HandleFunc("/tokenprice", TokenHandler)

	// Handle with DiverHandler
	bot.HandleFunc("/deepdive", DiverHandler)

	// Handle with MenuHandler
	/* 	bot.HandleFunc("/sendmenu", MenuHandler) */

	// Handle with MemeHandler
	bot.HandleFunc("/sendmeme", MemeHandler)

	// Handle with ShibeHandler
	bot.HandleFunc("/sendshibe", ShibeHandler)

	// Handle with DangerousHandler
	bot.HandleFunc("/itsdangerous", DangerousHandler)

	// Handle with AdviceHandler
	bot.HandleFunc("/advice", AdviceHandler)

	// Handle with AdviceHandler
	bot.HandleFunc("/insult", insultHandler)

	/* // Handler can accept varialbes// Handler can accept varialbes
	bot.HandleFunc("/say {text}", SayHandler)
	// Bot can send stickers, photos, music
	bot.HandleFunc("/sticker", StickerHandler)
	bot.HandleFunc("/photo", PhotoHandler)
	bot.HandleFunc("/keyboard", KeyboardHandler)

	// Use file handler to handle user uploads
	bot.HandleFile(FileHandler)
	bot.HandleFunc("/say {text}", SayHandler)
	// Bot can send stickers, photos, music
	bot.HandleFunc("/sticker", StickerHandler)
	bot.HandleFunc("/photo", PhotoHandler)
	bot.HandleFunc("/keyboard", KeyboardHandler)

	// Use file handler to handle user uploads
	bot.HandleFile(FileHandler) */

	// Set default handler if you want to process unmatched input
	bot.HandleDefault(EchoHandler)

	// Start listening for messages
	err = bot.ListenAndServe()
	log.Fatal(err)
}

// BiftHandler function for the Handler
func BiftHandler(message *tbot.Message) {
	// Handler can reply with several messages
	message.Reply("Ja! I bims!")
}

// DangerousHandler function for the Handler
func DangerousHandler(message *tbot.Message) {
	// Handler can reply with several messages
	message.Reply("It's dangerous to go alone take this!")
	message.ReplyPhoto("./pictures/dangerous.jpg")
}

// MemeHandler function for the Handler
func MemeHandler(message *tbot.Message) {
	// Handler can reply with several messages
	url := "http://inspirobot.me/api?generate=true"
	res, e := http.Get(url)
	resbytes, _ := ioutil.ReadAll(res.Body)
	if e != nil {
		log.Fatal(e)
	}
	defer res.Body.Close()

	resstr := string(resbytes)

	response, e := http.Get(resstr)

	file, err := os.Create("./pictures/meme.jpg")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if e != nil {
		log.Fatal(e)
	}

	message.ReplyPhoto("./pictures/meme.jpg")
}

// ShibeHandler function for the Handler
func ShibeHandler(message *tbot.Message) {
	// Handler can reply with several messages
	url := "http://shibe.online/api/shibes?count=1&urls=true"
	res, e := http.Get(url)
	resbytes, _ := ioutil.ReadAll(res.Body)
	if e != nil {
		log.Fatal(e)
	}
	defer res.Body.Close()

	var Shibe Shibe

	json.Unmarshal(resbytes, &Shibe)

	shibestr := (Shibe[0])

	response, e := http.Get(shibestr)

	file, err := os.Create("./pictures/shibe.jpg")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if err != nil {
		log.Fatal(err)
	}

	message.ReplyPhoto("./pictures/shibe.jpg")
}

// AdviceHandler function for the Advice
func AdviceHandler(message *tbot.Message) {
	url := "https://api.adviceslip.com/advice"
	res, _ := http.Get(url)
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	// Declaring Variables
	var advice Advice

	json.Unmarshal([]byte(body), &advice)
	/* if jsonErr != nil {
		log.Fatal(jsonErr)
	} */

	AdviceReply := "'" + advice.Slip.Advice + "'"

	message.Reply(AdviceReply)
}

func insultHandler(message *tbot.Message) {
	url := "https://evilinsult.com/generate_insult.php?lang=en&type=json"
	res, _ := http.Get(url)
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	// Declaring Variables
	var Insult Insult

	json.Unmarshal([]byte(body), &Insult)

	InsultReply := Insult.Insult

	message.Reply(InsultReply)
}

// TokenHandler function for the tokenprice
func TokenHandler(message *tbot.Message) {
	url := "https://eu.api.blizzard.com/data/wow/token/index?namespace=dynamic-eu&locale=en_US&access_token=USyn8bU8Ds4ZlFDm71Rqo4E2tJgzXs9M7s"
	res, _ := http.Get(url)
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	// Declaring Variables
	var Token Token

	// decoding json into the Token struct
	json.Unmarshal([]byte(body), &Token)

	// converting the Tokenprice into a string
	tokenpricestr := strconv.Itoa((Token.Price))

	// removing the trailing zeros on the pricestr
	tokenpriceshort := tokenpricestr[:len(tokenpricestr)-4]

	// Adding a dot after the 3rd character
	tokenprice := insertNth(tokenpriceshort, 3)

	// building the reply string
	reply := "The current Tokenprice on EU-Realms is: " + tokenprice + " Gold"

	message.Reply(reply)
}

// DiverHandler function for the deepdive
func DiverHandler(message *tbot.Message) {
	message.Reply("You are probably looking for this: https://tinder.com")
}

// EchoHandler for the error command function
func EchoHandler(message *tbot.Message) {
	message.Reply("I don't know that command")
}

/* func SayHandler(message *tbot.Message) {
	// Message contain it's varialbes from curly brackets
	message.Reply(message.Vars["text"])
} */

/* func StickerHandler(message *tbot.Message) {
	message.ReplySticker("sticker.png")
} */

/* func PhotoHandler(message *tbot.Message) {
	message.ReplyPhoto("photo.jpg", "it's me")
} */

/* func FileHandler(message *tbot.Message) {
	err := message.Download("./uploads")
	if err != nil {
		message.Replyf("Error handling file: %q", err)
		return
	}
	message.Reply("Thanks for uploading!")
} */
