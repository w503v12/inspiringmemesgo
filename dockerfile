FROM scratch

ADD ca-certificates.crt /etc/ssl/certs/

VOLUME ["/build"]

ADD inspiringmemesbot .
ADD src/pictures ./pictures
#RUN chmod +x inspiringmemesbot
CMD ["./inspiringmemesbot"]